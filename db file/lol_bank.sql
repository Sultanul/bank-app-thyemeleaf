-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 03:40 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lol_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `aId` int(11) NOT NULL,
  `aAddress` varchar(255) DEFAULT NULL,
  `aAmount` float NOT NULL,
  `aName` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`aId`, `aAddress`, `aAmount`, `aName`) VALUES
(101, 'Dhaka,Farmgate', 89700, 'sajib super mad'),
(95, 'Farmgate', 786000, 'rashed18'),
(94, 'Dhaka,Farmgate', 89700, 'sajib super'),
(90, 'gulshan', 88800, 'maien'),
(93, 'Dhaka,Farmgate', 789, 'abe12');

-- --------------------------------------------------------

--
-- Table structure for table `articlescomments`
--

CREATE TABLE `articlescomments` (
  `commentId` int(11) NOT NULL,
  `commentContent` varchar(255) DEFAULT NULL,
  `commentDate` varchar(255) DEFAULT NULL,
  `commentDownVote` int(11) NOT NULL,
  `commentUpVote` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `articleswithoutcomment`
--

CREATE TABLE `articleswithoutcomment` (
  `articleID` int(11) NOT NULL,
  `articleContent` varchar(255) DEFAULT NULL,
  `articleDate` varchar(255) DEFAULT NULL,
  `articleDownvote` int(11) NOT NULL,
  `articleSubject` varchar(255) DEFAULT NULL,
  `articleType` varchar(255) DEFAULT NULL,
  `articleUpvote` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `articles_articlescomments`
--

CREATE TABLE `articles_articlescomments` (
  `Articles_articleID` int(11) NOT NULL,
  `articlesComments_commentId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `bId` int(11) NOT NULL,
  `bName` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`bId`, `bName`) VALUES
(2, 'Uttara'),
(6, 'rajib'),
(12, 'farmgate'),
(91, 'asdad'),
(92, 'ajimpur');

-- --------------------------------------------------------

--
-- Table structure for table `branch_account`
--

CREATE TABLE `branch_account` (
  `Branch_bId` int(11) NOT NULL,
  `acountList_aId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(102),
(102),
(102);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`aId`);

--
-- Indexes for table `articlescomments`
--
ALTER TABLE `articlescomments`
  ADD PRIMARY KEY (`commentId`);

--
-- Indexes for table `articleswithoutcomment`
--
ALTER TABLE `articleswithoutcomment`
  ADD PRIMARY KEY (`articleID`);

--
-- Indexes for table `articles_articlescomments`
--
ALTER TABLE `articles_articlescomments`
  ADD UNIQUE KEY `UK_sdjba2dncamb6owonjdd0bw3x` (`articlesComments_commentId`),
  ADD KEY `FKfh4diqmwoywe4qqm41qk4qptf` (`Articles_articleID`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`bId`);

--
-- Indexes for table `branch_account`
--
ALTER TABLE `branch_account`
  ADD UNIQUE KEY `UK_qbqsvujyk5nh9skyjvg83yle4` (`acountList_aId`),
  ADD KEY `FK92r3w0cty99ylp4jbme5gcdhd` (`Branch_bId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
