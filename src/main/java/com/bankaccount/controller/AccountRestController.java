package com.bankaccount.controller;

import com.bankaccount.model.Account;
import com.bankaccount.model.AccountAmmountTransfer;
import com.bankaccount.model.Branch;
import com.bankaccount.model.Branch_model;
import com.bankaccount.model.AccountUnderBranch;
import com.bankaccount.service.Bank_Service_I;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Sajib
 */
@Controller
public class AccountRestController {

    @Autowired
    private Bank_Service_I bank_Service_I;
    //for bank app account section

    @RequestMapping(value = "/account_creation", method = RequestMethod.GET)
    public String account_Creation(Map<String, Object> model, Account acc) {
        model.put("acco", acc);
        List<Account> account_list = new ArrayList<>();
        account_list = bank_Service_I.account_list_read_all();
        model.put("accountList", account_list);
        return "account_creation";
    }

    @RequestMapping(value = "/account_creation_result", method = RequestMethod.POST)
    public String account_Creation_Result(@Valid Account accoun, Model model) {
        model.addAttribute("aId", accoun.getaId());
        model.addAttribute("name", accoun.getaName());
        model.addAttribute("address", accoun.getaAddress());
        model.addAttribute("amount", accoun.getaAmount());
        System.out.println("Before DB Save : " + accoun.toString());
        bank_Service_I.account_create(accoun);
        return "account_creation_result";
    }

    //for bank app branch section
    @RequestMapping("/branch_creation")
    public String branch_Creation(Map<String, Object> model, Branch bran) {
        model.put("branch", bran);
        List<Branch> branch_list = new ArrayList<>();
        branch_list = bank_Service_I.branch_list_read_all();
        model.put("branchList", branch_list);
        return "branch_creation";
    }

    @RequestMapping(value = "/branch_creation_result", method = RequestMethod.POST)
    public String branch_Creation_Result(@Valid Branch branc, Model model) {
        model.addAttribute("branchName", branc.getbName());
        model.addAttribute("accountList", branc.getAcountList());
        System.out.println("Before DB Save : " + branc.toString());
        bank_Service_I.branch_create(branc);
        return "branch_creation_result";
    }

    @RequestMapping(value = "/account_list_view", method = RequestMethod.GET)
    public String Account_list_view(Map<String, Object> model) {
        List<Account> account_list = new ArrayList<>();
        account_list = bank_Service_I.account_list_read_all();
        model.put("accountList", account_list);
        return "account_list_view";
    }

    @RequestMapping(value = "/branch_list_view", method = RequestMethod.GET)
    public String Branch_list_view(Map<String, Object> model) {
        List<Branch> branch_list = new ArrayList<>();
        branch_list = bank_Service_I.branch_list_read_all();
        model.put("branchList", branch_list);
        return "branch_list_view";
    }

    @RequestMapping(path = "/account/delete/{id}", method = RequestMethod.GET)
    public String account_Delete(@PathVariable(name = "id") int aId) {
        bank_Service_I.account_remove(aId);
//         List<Branch> branch_list = new ArrayList<>();
//        branch_list = bank_Service_I.branch_list_read_all();
//        model.put("branchList", branch_list);
//        
        return "redirect:/account_list_view";
    }

    @RequestMapping(value = "/account/edit/{id}", method = RequestMethod.GET)
    public String Account_edit(Model model, @PathVariable(value = "id") int id) {
        System.out.println("c-start");
        System.out.println("My Id: " + id);
        Account a = bank_Service_I.account_read_one(id);
        System.out.println("c1");
        System.out.println(a.toString());
        System.out.println("c2");
        model.addAttribute("account", a);
        System.out.println("c-end");
        return "account_edit";
    }

    @RequestMapping(value = "/account_edit_result", method = RequestMethod.POST)
    public String account_Edit_Result(@Valid Account account, Model model) {
        System.out.println("c_start");
        System.out.println("account id :" + account.getaId());
        model.addAttribute("aId", account.getaId());
        model.addAttribute("name", account.getaName());
        model.addAttribute("address", account.getaAddress());
        model.addAttribute("amount", account.getaAmount());
        System.out.println("Before DB Save : " + account.toString());
        bank_Service_I.account_update(account);
        return "account_edit_result";

    }

    @RequestMapping(value = "/branch/edit/{id}", method = RequestMethod.GET)
    public String Branch_edit(Model model, @PathVariable(value = "id") int id) {
        System.out.println("My Id: " + id);
        Branch b = bank_Service_I.branch_read_one(id);
        System.out.println(b.toString());

        model.addAttribute("branch", b);
        return "branch_edit";
    }

    @RequestMapping(value = "/branch_edit_result", method = RequestMethod.POST)
    public String branch_Edit_Result(@Valid Branch b, Model m) {
        System.out.println("branch id : " + b.getbId());
//        b.setaId(115);
        System.out.println("branch id : " + b.getbId());
        m.addAttribute("bId", b.getbId());
        m.addAttribute("name", b.getbName());
        m.addAttribute("branch_list", b.getAcountList());
        System.out.println("Before DB Save : " + b.toString());

        System.out.println("star : controler : before branch update");
        bank_Service_I.branch_update(b);
        System.out.println(b.toString());
        System.out.println("End: controler : After branch update");

        return "branch_edit_result";
    }

    @RequestMapping(path = "/branch/delete/{id}", method = RequestMethod.GET)
    public String branch_Delete(@PathVariable(name = "id") int bId) {
        System.out.println("before delete : " + bId);
        bank_Service_I.branch_remove(bId);
        System.out.println("sucessfully delete ");

        return "redirect:/branch_list_view";
    }

    @RequestMapping(path = "/branch/account_create/{id}", method = RequestMethod.GET)
    public String account_create_under_branch(@PathVariable(value = "id") int bId, 
            AccountUnderBranch accoutUnderBranch, Map<String, Object> model) {
//        bank_Service_I.creatAccountWithBranch(bId, acco);
        System.out.println("bid before" + bId);
        accoutUnderBranch.setbId(bId);
        model.put("accoutUnderBranch", accoutUnderBranch);
        model.put("brId", bId);
        System.out.println("bid after" + bId);

        return "create_account_under_branch";
    }

    @RequestMapping(value = "/create_account_under_branch_result", method = RequestMethod.POST)
    public String create_account_under_branch_result(AccountUnderBranch accoutUnderBranch, Model model, Map<String, Object> model_2) {
       
        int bId = accoutUnderBranch.getbId();
        Account account = accoutUnderBranch.getAccount();

        bank_Service_I.creatAccountWithBranch(bId, account);
        
        List<Branch> branch_list = new ArrayList<>();
        branch_list = bank_Service_I.branch_list_read_all();
        model_2.put("branchList", branch_list);

        return "branch_list_view";
    }
    
      @RequestMapping(value = "/balance_transfer", method = RequestMethod.GET)
      public String balance_Transfer(AccountAmmountTransfer accountAmmountTransfer,Map<String, Object> model, Account acc) {
        model.put("accountTransfermoney", accountAmmountTransfer);
        List<Account> account_list = new ArrayList<>();
        account_list = bank_Service_I.account_list_read_all();
        model.put("accountList", account_list);
        
        return "balance_transfer";
    }

     
    @RequestMapping(value = "/balance_transfer_m", method = RequestMethod.POST)
    public String transfer_Money(AccountAmmountTransfer accountAmmountTransfer, Map<String, Object> model) {
        int aid=accountAmmountTransfer.getAid();
        float transferMoney = accountAmmountTransfer.getTransferMoney();
        int aid1=accountAmmountTransfer.getAid1();
        bank_Service_I.transferMoney(aid, transferMoney, aid1);
        List<Account> account_list = new ArrayList<>();
        account_list = bank_Service_I.account_list_read_all();
        model.put("accountList", account_list);
        
       
        
        return "account_list_view";
    }
    
    
    
    
    
    
    
    
    

//    @RequestMapping(value = "/acub", method = RequestMethod.GET)
//    public String branch_under_account_Creation(Map<String, Object> model,int bId, Account acc) {
//        model.put("bid", bId);
//        model.put("account", acc);
//        
////        List<Account> account_list = new ArrayList<>();
////        account_list = bank_Service_I.account_list_read_all();
////        model.put("accountList", account_list);
//        return "branch_under_account_creation";
//    }
//    @RequestMapping(value = "/branch _under_account_creation_result", method = RequestMethod.POST)
//    public String branch_under_account_Creation_Result(int bId, Account accoun, Model model) {
//       model.addAttribute("bid", bId);
//        model.addAttribute("aId", accoun.getaId());
//        model.addAttribute("name", accoun.getaName());
//        model.addAttribute("address", accoun.getaAddress());
//        model.addAttribute("amount", accoun.getaAmount());
//        System.out.println("Before DB Save : " + accoun.toString());
//        bank_Service_I.creatAccountWithBranch(bId, accoun);
//        return "branch _under_account_creation_result";
//    }
}//class
