package com.bankaccount.model;

public class AccountUnderBranch {
       private int bId;
       private Account account;

    public int getbId() {
        return bId;
    } 


    public void setbId(int bId) {
        this.bId = bId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "AccountUnderBranch{" + "bId=" + bId + ", account=" + account + '}';
    }
    
}
