package com.bankaccount.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

public class Branch_model
{

    private int bId;
    
    private String bName;
    

    private List<Account> acountList;

    public Branch_model() {
    }

    public Branch_model(int bId, String bName) {
        this.bId = bId;
        this.bName = bName;
    }
    
       public Branch_model(String bName, List acountList) {
       
        this.bName = bName;
        this.acountList = acountList;
    }
    
    public Branch_model(int bId, String bName, List acountList) {
        this.bId = bId;
        this.bName = bName;
        this.acountList = acountList;
    }
    
    
    
    public Branch_model(String bName) 
    {
        this.bName = bName;
    }

    public int getbId() 
    {
        return bId;
    }

    public void setaId(int bId) 
    {
        this.bId = bId;
    }

    public String getbName() 
    {
        return bName;
    }

    public void setbName(String bName) 
    {
        this.bName = bName;
    }

    public List getAcountList() 
    {
        return acountList;
    }

    public void setAcountList(List acountList) 
    {
        this.acountList = acountList;
    }

    @Override
    public String toString() 
    {
        return "Branc_model : {" + "bId=" + bId + 
                       ", bName=" + bName + 
                       ", acountList=" + acountList + 
                       '}';
    }
    
    
}
