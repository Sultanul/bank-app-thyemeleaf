package com.bankaccount.model;

public class AccountAmmountTransfer {
    private int aid;
    private float transferMoney;
    private int aid1;

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public float getTransferMoney() {
        return transferMoney;
    }

    public void setTransferMoney(float transferMoney) {
        this.transferMoney = transferMoney;
    }

    public int getAid1() {
        return aid1;
    }

    public void setAid1(int aid1) {
        this.aid1 = aid1;
    }

    @Override
    public String toString() {
        return "AccountAmmountTransfer{" + "aid=" + aid + ", transferMoney=" + transferMoney + ", aid1=" + aid1 + '}';
    }
    
    
}
